class BillLogsController < ApplicationController
	def create
		@billLog = BillLog.new(bill_log_params)

		#update the current balance 
		@report_bill = Balance.where(user_id: current_user.id).last.available_balance
		@bill_to_current = @report_bill.to_f - @billLog.amount_spent.to_f
		Balance.new(available_balance: @bill_to_current, user_id: current_user.id).save 

		#updated the amount in the bill Trust
		@bill_trust_balance = Bill.where(user_id: current_user.id).last.available_balance
		@bill_trust_update = @bill_trust_balance.to_f - @billLog.amount_spent.to_f
		Bill.new(available_balance: @bill_trust_update, user_id: current_user.id, day: Time.now).save
		
		#save the new bill log
		@billLog.user_id = current_user.id
		@billLog.date_posted = Time.now
		@billLog.updated_balance = @bill_to_current
		@billLog.bill_id = Bill.find_by(user_id: current_user.id).id
		if @billLog.save
			flash[:success] = "Updated your Trust"
			redirect_to current_user
		else
			flash[:error] = "Unable to updated your Trust"
			redirect_to current_user
		end
	end

	def edit
		@billLog = BillLog.find(params[:id])
	end

	def update
	end

	private
	def bill_log_params
		params.require(:bill_log).permit(:amount_spent, :type_of_spending)
	end
end

