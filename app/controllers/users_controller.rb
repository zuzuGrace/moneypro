class UsersController < ApplicationController
	def show
		@user = User.find(params[:id])
		@billLog = BillLog.all
		@enrichLog = EnrichmentLog.all
		@investLog = InvestmentLog.all
		@necLog = NecessityLog.all
		@philLog = PhilanthropyLog.all
		@secLog = SecurityLog.all
		@whimLog = WhimLog.all
		@necessities = Necessity.all
		@bill = Bill.all
		@encrich = Enrichment.all
		@daily = Whim.all
		@long = Investment.all
		@cap = Philanthropy.all
		@save = Security.all
		@fa = FinancialAssistant.all
	end

	def create
		#pay for the service
		@amount = 1099
		customer = Stripe::Customer.create({
			:email => params[:user][:email],
			:source => params[:stripeToken]

		})

		

		@charge = Stripe::Charge.create({
			:customer => customer.id,
			:amount => @amount,
			:description => "MoneyPro", 
			:currency => 'usd'
		})

		Stripe::Subscription.create({
			:customer => customer.id,
			:items => [{
				#:plan => "plan_DrJHXahjwajqzU"
				:plan => 'plan_Ez4pfkxuOMj57o'
				}]
		})

		
		if !@charge.nil? 
			@user = User.new(user_params)
			@user.stripe_customer_id = customer.id
			@user.paid = true
			@user.first_time = true
			if @user.save
				#@user.send_activation_email
      			flash[:success] = "Please check your email to activate your account."

				log_in @user
				redirect_to @user
			else
				redirect_to root_url
				flash[:error] = "Unable to create your account. Please try again."
			end
		end

		rescue Stripe::CardError => e
			flash[:error] = e.message
			redirect_to root_url
		end
	end



	private

		def user_params
			params.require(:user).permit(:firstname, :lastname, :email, :password, :password_confirmation)
		end


