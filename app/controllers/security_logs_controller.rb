class SecurityLogsController < ApplicationController
	def create
		@secLog = SecurityLog.new(sec_log_params)

		#update the current balance 
		@report_sec = Balance.where(user_id: current_user.id).last.available_balance
		@sec_to_current = @report_sec.to_f - @secLog.amount_spent.to_f
		Balance.new(available_balance: @sec_to_current, user_id: current_user.id).save 

		#updated the amount in the Security Trust
		@sec_trust_balance = Security.where(user_id: current_user.id).last.available_balance
		@sec_trust_update = @sec_trust_balance.to_f - @secLog.amount_spent.to_f
		Security.new(available_balance: @sec_trust_update, user_id: current_user.id, day: Time.now).save
		
		#save the new Security log
		@secLog.user_id = current_user.id
		@secLog.date_posted = Time.now
		@secLog.updated_balance = @sec_to_current
		@secLog.security_id = Security.find_by(user_id: current_user.id).id
		if @secLog.save
			flash[:success] = "Updated your Trust"
			redirect_to current_user
		else
			flash[:error] = "Unable to updated your Trust"
			redirect_to current_user
		end
	end


	private
	def sec_log_params
		params.require(:security_log).permit(:amount_spent, :type_of_spending)
	end
end
