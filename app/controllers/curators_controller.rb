class CuratorsController < ApplicationController
	def show
		@curator = Curator.find(params[:id])
		@users = User.all
		@instructions = FinancialAssistant.all 
	end

	def new
		@curator = Curator.new
	end

	def create
		@curator = Curator.new(curator_params)
		if @curator.save
			flash[:success] = "Created new curator"
			redirect_to current_curator
		else
			flash[:error] = "Unable to create new curator"
		end
	end

	
	def edit
		@edit_curator = Curator.find(params[:id])
	end

	def update
		@curator = Curator.find(params[:id])
		if @curator.update(curator_params)
			flash[:success] = "Updated your preferences"
		else 
			flash[:error] = "Unable to update"
		end
	end


	private
	def curator_params
		params.require(:curator).permit(:username, :password, :password_confirmation)
	end
	
end
