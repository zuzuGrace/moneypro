class NecessityLogsController < ApplicationController
	def create
		@necLog = NecessityLog.new(nec_log_params)

		#update the current balance 
		@report_nec = Balance.where(user_id: current_user.id).last.available_balance
		@nec_to_current = @report_nec.to_f - @necLog.amount_spent.to_f
		Balance.new(available_balance: @nec_to_current, user_id: current_user.id).save 

		#updated the amount in the Necessity Trust
		@nec_trust_balance = Necessity.where(user_id: current_user.id).last.available_balance
		@nec_trust_update = @nec_trust_balance.to_f - @necLog.amount_spent.to_f
		Necessity.new(available_balance: @nec_trust_update, user_id: current_user.id, day: Time.now).save
		
		#save the new Necessity log
		@necLog.user_id = current_user.id
		@necLog.date_posted = Time.now
		@necLog.updated_balance = @nec_to_current
		@necLog.necessity_id = Necessity.find_by(user_id: current_user.id).id
		if @necLog.save
			flash[:success] = "Updated your Trust"
			redirect_to current_user
		else
			flash[:error] = "Unable to updated your Trust"
			redirect_to current_user
		end
	end


	private
	def nec_log_params
		params.require(:nec).permit(:amount_spent, :type_of_spending)
	end
end
