class WhimLogsController < ApplicationController
	def create
		@wLog = WhimLog.new(whim_log_params)

		#update the current balance 
		@report_whim = Balance.where(user_id: current_user.id).last.available_balance
		@whim_to_current = @report_whim.to_f - @wLog.amount_spent.to_f
		Balance.new(available_balance: @whim_to_current, user_id: current_user.id).save 

		#updated the amount in the Whim Trust
		@whim_trust_balance = Whim.where(user_id: current_user.id).last.available_balance
		@whim_trust_update = @whim_trust_balance.to_f - @wLog.amount_spent.to_f
		Whim.new(available_balance: @whim_trust_update, user_id: current_user.id, day: Time.now).save
		
		#save the new Whim log
		@wLog.user_id = current_user.id
		@wLog.date_posted = Time.now
		@wLog.updated_balance = @whim_to_current
		@wLog.whim_id = Whim.find_by(user_id: current_user.id).id
		if @wLog.save
			flash[:success] = "Updated your Trust"
			redirect_to current_user
		else
			flash[:error] = "Unable to updated your Trust"
			redirect_to current_user
		end
	end

	private
	def whim_log_params
		params.require(:whim).permit(:amount_spent, :type_of_spending)
	end
end