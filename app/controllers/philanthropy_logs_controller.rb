class PhilanthropyLogsController < ApplicationController
	def create
		@pLog = PhilanthropyLog.new(phil_log_params)

		#update the current balance 
		@report_p = Balance.where(user_id: current_user.id).last.available_balance
		@p_to_current = @report_p.to_f - @pLog.amount_spent.to_f
		Balance.new(available_balance: @p_to_current, user_id: current_user.id).save 

		#updated the amount in the Necessity Trust
		@p_trust_balance = Philanthropy.where(user_id: current_user.id).last.available_balance
		@p_trust_update = @p_trust_balance.to_f - @pLog.amount_spent.to_f
		Philanthropy.new(available_balance: @p_trust_update, user_id: current_user.id, day: Time.now).save
		
		#save the new Philanthropy log
		@pLog.user_id = current_user.id
		@oLog.date_posted = Time.now
		@pLog.updated_balance = @p_to_current
		@pLog.philanthropy_id = Philanthropy.find_by(user_id: current_user.id).id
		if @pLog.save
			flash[:success] = "Updated your Trust"
			redirect_to current_user
		else
			flash[:error] = "Unable to updated your Trust"
			redirect_to current_user
		end
	end


	private
	def phil_log_params
		params.require(:philanthropy_log).permit(:amount_spent, :type_of_spending)
	end
end
