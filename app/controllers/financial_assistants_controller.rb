class FinancialAssistantsController < ApplicationController
def index
	@fa = FinancialAssistant.all
end

def new
end

def create
	@fa = FinancialAssistant.new(fa_params)
	if @fa.save
		flash[:success] = "Saved to Database"
		redirect_to current_curator
	else 
		flash[:error] = "An error occured"
	end
end

def edit
	@fa = FinancialAssistant.find(params[:id])
end

def update
	@fa = FinancialAssistant.find(params[:id])
	if @fa.update(fa_params)
		flash[:success] = "Successfully updated the Financial Assistant"
	else 
		flash[:error] = "Could not update the financial assistant"
	end
end

def destroy
	@fa = FinancialAssistant.find(params[:id])
	@fa.destroy
	redirect_to current_curator
end

private
def fa_params
	params.require(:fa).permit(:note, :numbered_value, :action)
end

end
