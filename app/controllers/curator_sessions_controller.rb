class CuratorSessionsController < ApplicationController
  def new
  end

  def create
  	curate = Curator.find_by(username: params[:curator_session][:username].downcase)
  	if curate && curate.authenticate(params[:curator_session][:password])
  		log_in_curator curate
  		redirect_to curate
  	else
  		flash[:error] = "Unable to Log In"
  		render 'new'
  	end
  end

  def destroy
    log_out_curator
    redirect_to root_url
  end
end
