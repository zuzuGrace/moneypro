class EnrichmentLogsController < ApplicationController
	def create
		@enrichLog = EnrichmentLog.new(enrich_log_params)

		#update the current balance 
		@report_enrich = Balance.where(user_id: current_user.id).last.available_balance
		@enrich_to_current = @report_enrich.to_f - @enrichLog.amount_spent.to_f
		Balance.new(available_balance: @enrich_to_current, user_id: current_user.id).save 

		#updated the amount in the enrichment Trust
		@enrich_trust_balance = Enrichment.where(user_id: current_user.id).last.available_balance
		@enrich_trust_update = @enrich_trust_balance.to_f - @enrichLog.amount_spent.to_f
		Enrichment.new(available_balance: @enrich_trust_update, user_id: current_user.id, day: Time.now).save
		
		#save the new enrichment log
		@enrichLog.user_id = current_user.id
		@enrichLog.date_posted = Time.now
		@enrichLog.updated_balance = @enrich_to_current
		@enrichLog.enrichment_id = Enrichment.find_by(user_id: current_user.id).id
		if @enrichLog.save
			flash[:success] = "Updated your Trust"
			redirect_to current_user
		else
			flash[:error] = "Unable to updated your Trust"
			redirect_to current_user
		end
	end

	def edit
		@enrichLog = EnrichmentLog.find(params[:id])
	end

	def update
	end

	private
	def enrich_log_params
		params.require(:enrich).permit(:amount_spent, :type_of_spending)
	end
end
