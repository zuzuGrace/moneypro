class BalancesController < ApplicationController
	def index
		@balances = Balance.all
	end

	def create
		@balance = Balance.new(balance_params)
		@balance.user_id = current_user.id
		@seventy_trust_share = @balance.available_balance.to_f * 0.70
		@seventy_trust_fourth = @seventy_trust_share * 0.25
		@thirty_trust_share = @balance.available_balance.to_f * 0.30
		@thirty_trust_third = @thirty_trust_share
		
		#this balance is reported to all trusts comes in as string has to be converted to decimal so tha math can be do appropriately
		Enrichment.new(available_balance: @seventy_trust_fourth, user_id: current_user.id, day: Time.now ).save	
		Bill.new(available_balance: @seventy_trust_fourth, user_id: current_user.id, day: Time.now ).save	
		Necessity.new(available_balance: @seventy_trust_fourth, user_id: current_user.id, day: Time.now ).save
		Whim.new(available_balance: @seventy_trust_fourth, user_id: current_user.id, day: Time.now ).save	
		Investment.new(available_balance: @thirty_trust_third, user_id: current_user.id, day: Time.now ).save	
		Philanthropy.new(available_balance: @thirty_trust_third, user_id: current_user.id, day: Time.now ).save
		Security.new(available_balance: @thirty_trust_third, user_id: current_user.id, day: Time.now ).save


		#update log

		EnrichmentLog.new(amount_spent: @seventy_trust_fourth, user_id: current_user.id, updated_balance: @seventy_trust_fourth, type_of_spending: "Funds Added to Trust", date_posted: Time.now, enrichment_id: Enrichment.find_by(user_id: current_user.id).id ).save	
		BillLog.new(amount_spent: @seventy_trust_fourth, user_id: current_user.id, updated_balance: @seventy_trust_fourth, type_of_spending: "Funds Added to Trust", date_posted: Time.now, bill_id: Bill.find_by(user_id: current_user.id).id).save
		NecessityLog.new(amount_spent: @seventy_trust_fourth, user_id: current_user.id, updated_balance: @seventy_trust_fourth, type_of_spending: "Funds Added to Trust", date_posted: Time.now, necessity_id: Necessity.find_by(user_id: current_user.id).id).save
		InvestmentLog.new(amount_spent: @seventy_trust_fourth, user_id: current_user.id, updated_balance: @seventy_trust_fourth, type_of_spending: "Funds Added to Trust", date_posted: Time.now, investment_id: Investment.find_by(user_id: current_user.id).id).save
		PhilanthropyLog.new(amount_spent: @thirty_trust_third, user_id: current_user.id, updated_balance: @thirty_trust_third, type_of_spending: "Funds Added to Trust", date_posted: Time.now, philanthropy_id:  Philanthropy.find_by(user_id: current_user.id).id).save
		SecurityLog.new(amount_spent: @thirty_trust_third, user_id: current_user.id, updated_balance: @thirty_trust_third, type_of_spending: "Funds Added to Trust", date_posted: Time.now, security_id:Security.find_by(user_id: current_user.id).id).save
		WhimLog.new(amount_spent: @thirty_trust_third, user_id: current_user.id, updated_balance: @thirty_trust_third, type_of_spending: "Funds Added to Trust", date_posted: Time.now, whim_id: Whim.find_by(user_id: current_user.id).id).save
		if @balance.save
			flash[:success] = "Updated Balance"
			redirect_to current_user
		else 
			flash[:error] = "Unable to update your balance. Please try again"
		end
	end

	def edit
	end

	def update
	end


	private
		def balance_params
			params.require(:balance).permit(:available_balance)
		end


end
