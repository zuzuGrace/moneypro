class InvestmentLogsController < ApplicationController
	def create	
		@investLog = InvestmentLog.new(invest_log_params)

		#update the current balance 
		@report_invest = Balance.where(user_id: current_user.id).last.available_balance
		@invest_to_current = @report_invest.to_f - @investLog.amount_spent.to_f
		Balance.new(available_balance: @invest_to_current, user_id: current_user.id).save 

		#updated the amount in the investment Trust
		@invest_trust_balance = Investment.where(user_id: current_user.id).last.available_balance
		@invest_trust_update = @invest_trust_balance.to_f - @investLog.amount_spent.to_f
		Investment.new(available_balance: @invest_trust_update, user_id: current_user.id, day: Time.now).save
		
		#save the new investment log
		@investLog.user_id = current_user.id
		@investLog.date_posted = Time.now
		@investLog.updated_balance = @invest_to_current
		@investLog.investment_id = Investment.find_by(user_id: current_user.id).id
		if @investLog.save
			flash[:success] = "Updated your Trust"
			redirect_to current_user
		else
			flash[:error] = "Unable to updated your Trust"
			redirect_to current_user
		end
	end


	private
	def invest_log_params
		params.require(:invest).permit(:amount_spent, :type_of_spending)
	end
end
