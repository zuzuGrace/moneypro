class Whim < ApplicationRecord
  belongs_to :user
  has_many :whim_logs
  validates :available_balance, presence: true
end
