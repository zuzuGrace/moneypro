class PhilanthropyLog < ApplicationRecord
  belongs_to :philanthropy
    validates :amount_spent, presence: true
  validates :type_of_spending, presence: true
end
