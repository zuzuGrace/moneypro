class EnrichmentLog < ApplicationRecord
  belongs_to :enrichment
    validates :amount_spent, presence: true
  validates :type_of_spending, presence: true
end
