class Curator < ApplicationRecord
	before_save {username.downcase!}
	VALID_USERNAME = /@[a-z\d\-]/
	validates :username, presence: true, length: {maximum: 75}, format: {with: VALID_USERNAME}, uniqueness: true

	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }

	# Returns the hash digest of the given string.
  def Curator.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

end
