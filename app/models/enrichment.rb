class Enrichment < ApplicationRecord
  belongs_to :user
  has_many :enrichment_logs
  validates :available_balance, presence: true
end
