class InvestmentLog < ApplicationRecord
  belongs_to :investment
    validates :amount_spent, presence: true
  validates :type_of_spending, presence: true
end
