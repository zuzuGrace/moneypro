class WhimLog < ApplicationRecord
  belongs_to :whim
  validates :amount_spent, presence: true
  validates :type_of_spending, presence: true
end
