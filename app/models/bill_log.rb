class BillLog < ApplicationRecord
  belongs_to :bill
  validates :amount_spent, presence: true
  validates :type_of_spending, presence: true

end
