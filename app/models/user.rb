class User < ApplicationRecord
  has_many :bill, dependent: :destroy
  has_many :enrichment, dependent: :destroy
  has_many :necessity, dependent: :destroy
  has_many :philanthropy, dependent: :destroy
  has_many :security, dependent: :destroy
  has_many :whim, dependent: :destroy
  has_many :balances, dependent: :destroy
  
	attr_accessor :remember_token, :activation_token,  :reset_token
	before_save :downcase_email
  before_create :create_activation_digest
	validates :firstname, presence: true, length: {maximum: 50}
	validates :lastname, presence: true, length: {maximum: 50}
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  	validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }

    def User.digest(string)
    	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
  	end

  	def User.new_token
    	SecureRandom.urlsafe_base64
  	end

  	def remember
    	self.remember_token = User.new_token
    	update_attribute(:remember_digest, User.digest(remember_token))
  	end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  	def forget
    	update_attribute(:remember_digest, nil)
  	end


    # Activates an account.
  def activate
    update_columns(activated: FILL_IN, activated_at: FILL_IN)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

   # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest:  FILL_IN, reset_sent_at: FILL_IN)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

   def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

    private
    def downcase_email
      self.email = email.downcase
    end


    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
end
