class NecessityLog < ApplicationRecord
  belongs_to :necessity
    validates :amount_spent, presence: true
  validates :type_of_spending, presence: true
end
