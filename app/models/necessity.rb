class Necessity < ApplicationRecord
  belongs_to :user
  has_many :necessity_logs
  validates :available_balance, presence: true
end
