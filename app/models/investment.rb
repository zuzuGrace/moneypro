class Investment < ApplicationRecord
  belongs_to :user
  has_many :investment_logs
  validates :available_balance, presence: true
end
