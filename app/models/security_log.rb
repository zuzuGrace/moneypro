class SecurityLog < ApplicationRecord
  belongs_to :security
    validates :amount_spent, presence: true
  validates :type_of_spending, presence: true
end
