class Philanthropy < ApplicationRecord
  belongs_to :user
  has_many :philanthropy_logs
  validates :available_balance, presence: true
end
