class Bill < ApplicationRecord
  belongs_to :user
  has_many :bill_logs
  validates :available_balance, presence: true
end
