class Security < ApplicationRecord
  belongs_to :user
  has_many :security_logs
  validates :available_balance, presence: true
end
