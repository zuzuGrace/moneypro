module CuratorSessionsHelper
	  def log_in_curator(curator)
    	session[:curator_id] = curator.id
    end

  	  def current_curator
        if session[:curator_id]
          @current_curator ||= Curator.find_by(id: session[:curator_id])
        end
      end

  #returns true if the curator is logged in, false otherwise
  def curator_logged_in
  	!current_curator.nil?
  end

  #log out the curator
  def log_out_curator
    session.delete(:curator_id)
    @current_curator = nil?
  end

end
