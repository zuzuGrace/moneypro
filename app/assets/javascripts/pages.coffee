# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
			$('.footer-link').mouseenter ->
				$(this).css("color", "#EDE9EB")
				$('.link-content').fadeOut(250)
				$('.home-page-slider').fadeOut(250)
				if @id == "blue"
					$(this).css("letter-spacing", "4px")
					$('#blueberry').fadeIn(550)
				else if @id == "how"
					$(this).css("letter-spacing", "4px")
					$('#work').fadeIn(550)
				else if @id == "up"
					$(this).css("letter-spacing", "4px")
					$('#Sign').fadeIn(550)
				else if @id == "in"
					$(this).css("letter-spacing", "4px")
					$('#Log').fadeIn(550)

			$('.signupbttn').mouseenter ->
				$(this).css("background-color", "#C64427")
				$(this).css("color", "white")
				$('.link-content').fadeOut(250)
				$('.home-page-slider').fadeOut(250)
				$('#Sign').fadeIn(550)

			$('.signupbttn').mouseleave ->
				$(this).css("color", "#C64427")
				$(this).css("background-color", "white")

			$('.footer-link').mouseleave ->
				$(this).css("color", "#559BA8")
				$(this).css("letter-spacing", "1px")

			setTimeout ( ->
				$('.alert-success').remove()
				return
			), 10000

			setTimeout ( ->
				$('.alert-danger').remove()
				return
			), 10000

			setTimeout ( ->
				$('.alert-error').remove()
				return
			), 10000


			$('#mp').mouseenter ->
					$(this).css("transform", "scale(1.2)")
					$('#mp_parag').css("opacity", "1")

			$('#mp').mouseleave ->
					$(this).css("transform", "scale(1)")
					$('#mp_parag').css("opacity", "0.20")

			$('#home_seven').mouseenter ->
					$(this).css("transform", "scale(1.05)")
					$('#sev_parag').css("opacity", "1")
					$('#seven_item').css("opacity", "1")
			$('#home_seven').mouseleave ->
					$(this).css("transform", "scale(1)")
					$('#sev_parag').css("opacity", "0.20")
					$('#seven_item').css("opacity", "0.20")
			$('#home_thirty').mouseenter ->
					$(this).css("transform", "scale(1.05)")
					$('#th_parag').css("opacity", "1")
					$('#thirty_item').css("opacity", "1")
			$('#home_thirty').mouseleave ->
					$(this).css("transform", "scale(1)")
					$('#th_parag').css("opacity", "0.20")
					$('#thirty_item').css("opacity", "0.20")


			 slideIndex = 0
		

			 carosouel = ->
			 	i = 0
			 	x = document.getElementsByClassName("home-pg")
			 	while i < x.length  
			 		x[i].style.display = "none"
			 		i++
			 	slideIndex++
			 	if slideIndex > x.length
			 		slideIndex = 1
			 	x[slideIndex-1].style.display = "block"
			 	setTimeout(carosouel, 8000)

			 do carosouel