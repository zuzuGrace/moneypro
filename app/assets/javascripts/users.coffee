# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
			$('.dash-links').mouseenter ->	
				$('.dashOpts').css("letter-spacing", "0px")
				$('.dash-links').css("color", "#559BA8")
				$('.dash-links').css("letter-spacing", "0px")
				$(this).css("color", "white")
				$(this).css("letter-spacing", "3px")
				$('.views').fadeOut(150)
				$('.detailed_spending').fadeOut(150)
				$('.viewd').fadeOut(150)
				$('.view-balance').fadeOut(150)
				$('.view-report').fadeOut(150)
				$('.close-img').fadeOut()
				$('.main-imgs').fadeOut()
				$('.popUpOut').fadeOut(150)
				if @id == "current"
					$('#current-view').fadeIn(550)
				else if @id == "hiw"
					$('#close_hiw').fadeIn(560)
					$('#htw-view').fadeIn(550)
				else if @id == "neces"
					$('#close_nec').fadeIn(560)
					$('#necessities_trust_expenditures').fadeIn(550)
					$('#nec-view').fadeIn(550)
				else if @id == "bills"
					$('#close_bill').fadeIn(560)
					$('#bill_trust_expenditures').fadeIn(550)
					$('#bills-view').fadeIn(550)
				else if @id == "enrich"
					$('#close_enrich').fadeIn(560)
					$('#enrich_trust_expenditures').fadeIn(550)
					$('#enrich-view').fadeIn(550)
				else if @id == "daily"
					$('#close_daily').fadeIn(560)
					$('#whims_trust_expenditures').fadeIn(550)
					$('#daily-view').fadeIn(550)
				else if @id == "long_term"
					$('#close_long').fadeIn(560)
					$('#investment_trust_expenditures').fadeIn(550)
					$('#long-view').fadeIn(550)
				else if @id == "philan"
					$('#close_phil').fadeIn(560)
					$('#philanthropy_trust_expenditures').fadeIn(550)
					$('#philan-view').fadeIn(550)
				else if @id == "security"
					$('#close_save').fadeIn(560)
					$('#securities_trust_expenditures').fadeIn(550)
					$('#savings-view').fadeIn(550)




			$('.dashOpts').click -> 
				$(this).css("letter-spacing", "4px")
				$('.views').fadeOut(150)
				$('.viewd').fadeOut(150)
				$('.close-img').fadeOut()
				$('.main-imgs').fadeOut()
				$('.view-balance').fadeOut(150)
				$('.view-report').fadeOut(150)
				if @id == 'bUpdate'
					$('#b').fadeIn(550)
					$('#rTransactions').css("letter-spacing", "0px")
				else if @id == 'rTransactions'
					$('#s').fadeIn(550)
					$('#bUpdate').css("letter-spacing", "0px")

		
				

			$('.avail_bal').click ->
				if @id == 'navail'
					$('#nec-amt').fadeIn(550)
				else if @id == 'bavail'
					$('#bills-amt').fadeIn(550)
				else if @id == 'eavail'
					$('#enrich-amt').fadeIn(550)
				else if @id == 'davail'
					$('#daily-amt').fadeIn(550)
				else if @id == 'ltavail'
					$('#long-amt').fadeIn(550)
				else if @id == 'phavail'
					$('#philan-amt').fadeIn(550)
				else if @id == 'savail'
					$('#savings-amt').fadeIn(550)

			$('button').click ->
				$('.update_trust_amt').hide()


			$('#close_nec').click ->
				$(this).hide()
				$('#nec-view').animate({
					width: "5%"
				}, 2000)
				$('#expandn').fadeIn(4100)

			$('#close_bill').click ->
				$(this).hide()
				$('#bills-view').animate({
					width: "5%"
				}, 2000)
				$('#expandb').fadeIn(4100)

			$('#close_enrich').click ->
				$(this).hide()
				$('#enrich-view').animate({
					width: "5%"
				}, 2000)
				$('#expande').fadeIn(4100)


			$('#close_daily').click ->
				$(this).hide()
				$('#daily-view').animate({
					width: "5%"
				}, 2000)
				$('#expandd').fadeIn(4100)

			$('#close_long').click ->
				$(this).hide()
				$('#long-view').animate({
					width: "5%"
				}, 2000)
				$('#expandl').fadeIn(4100)

			$('#close_phil').click ->
				$(this).hide()
				$('#philan-view').animate({
					width: "5%"
				}, 2000)
				$('#expandp').fadeIn(4100)

			$('#close_save').click ->
				$(this).hide()
				$('#savings-view').animate({
					width: "5%"
				}, 2000)
				$('#expands').fadeIn(4100)

			$('#close_hiw').click ->
				$(this).hide()
				$('#htw-view').animate({
					width: "5%"
				}, 2000)
				$('#expandh').fadeIn(4100)



			
			$('#expandh').click ->
				$(this).hide()
				$('#htw-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_hiw').fadeIn(4100)

			$('#expandn').click ->
				$(this).hide()
				$('#nec-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_nec').fadeIn(4100)

			$('#expandb').click ->
				$(this).hide()
				$('#bills-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_bill').fadeIn(4100)

			$('#expande').click ->
				$(this).hide()
				$('#enrich-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_enrich').fadeIn(4100)

			$('#expandd').click ->
				$(this).hide()
				$('#daily-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_daily').fadeIn(4100)

			$('#expandl').click ->
				$(this).hide()
				$('#long-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_long').fadeIn(4100)
			$('#expandp').click ->
				$(this).hide()
				$('#philan-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_phil').fadeIn(4100)

			$('#expands').click ->
				$(this).hide()
				$('#savings-view').animate({
					width: "74.5%"
				}, 2000)
				$('#close_save').fadeIn(4100)

			$('#close_fa').click ->
				$('#fa_interact').fadeOut(250)

			$('#fa_avatar').mouseenter ->
				$(this).css("transform", "scale(1.15)")

			$('#fa_avatar').mouseleave ->
				$(this).css("transform", "scale(1)")

			$('#fa_avatar').click ->
				$('#fa_interact').fadeIn(550)

			$('#orient').click ->
				$('#orient_box').fadeIn(550)
				$('#overlay').fadeIn(100)

			$('#overlay').click ->
				$('#orient_box').fadeOut()
				$(this).fadeOut()


			document.getElementById('play_orientation').innerHTML = ''
			i= 0
			txt = ["Hello, welcome to your MoneyPro dashboard.", "Please start by reporting your funds.", "This will allow MoneyPro to show spread them to all your Trusts and give you the funds available to cover each Trust.", "Click Update Total INCOME in the top right corner to start.", "Once your funds are updated, they will be distributed to all Seven Trusts allowing you to know how much you can spend from each Trust.", "Click the Amount beneath each Trust to report a transaction.", "It is important to report transactions so that you can track your spending.", "You can view all reported transactions by clicking the Repport Transactions link at the top right or the Trust's link on the left side of the screen."]
			speed = 5000
			typeWriter = ->
				if i < txt.length
					document.getElementById('play_orientation').innerHTML = txt[i]
					i++
					setTimeout typeWriter, speed
				return

			do typeWriter






					
