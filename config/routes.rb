Rails.application.routes.draw do
  root 'pages#home'

  post 'update', to: 'balances#create'

  post 'report_bill_transaction', to: 'bill_logs#create'
  post 'report_enrich_transaction', to: 'enrichment_logs#create'
  post 'report_necessities_transaction', to: 'necessity_logs#create'
  post 'report_whims_transaction', to: 'whim_logs#create'
  post 'report_invest_transaction', to: 'investment_logs#create'
  post 'report_cap_transaction', to: 'philanthropy_logs#create'
  post 'report_savings_transaction', to: 'security_logs#create'

  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/curate', to: 'curator_sessions#new'
  post '/curate', to: 'curator_sessions#create'
  delete '/close', to: 'curator_sessions#destroy'

  post '/fa', to: 'financial_assistants#create'

  resources :users do
  	resources :necessities do
  		resources :necessity_logs 
  	end
  	resources :bills do 
  		resources :bill_logs
  	end
  	resources :enrichments do
  		resources :enrichment_logs
  	end
  	resources :investments do 
  		resources :investment_logs
  	end
  	resources :philanthropies do 
  		resources :philanthropy_logs
  	end
  	resources :securities do
  		resources :security_logs
  	end
  	resources :whims do 
  		resources :whim_logs
  	end

    resources :balances
  end

  resources :financial_assistants
  resources :curators




  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
