class CreatePhilanthropies < ActiveRecord::Migration[5.2]
  def change
    create_table :philanthropies do |t|
      t.references :user, foreign_key: true
      t.decimal :available_balance

      t.timestamps
    end
  end
end
