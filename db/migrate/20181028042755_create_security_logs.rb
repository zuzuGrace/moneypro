class CreateSecurityLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :security_logs do |t|
      t.references :security, foreign_key: true
      t.string :amount_spent
      t.string :type_of_spending
      t.string :updated_balance
      t.datetime :date_posted
      t.integer :user_id

      t.timestamps
    end
  end
end
