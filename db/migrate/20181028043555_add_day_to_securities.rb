class AddDayToSecurities < ActiveRecord::Migration[5.2]
  def change
    add_column :securities, :day, :datetime
  end
end
