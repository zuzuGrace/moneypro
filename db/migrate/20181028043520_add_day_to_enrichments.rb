class AddDayToEnrichments < ActiveRecord::Migration[5.2]
  def change
    add_column :enrichments, :day, :datetime
  end
end
