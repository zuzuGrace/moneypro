class AddCurrentBalanceToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :balance, :string
  end
end
