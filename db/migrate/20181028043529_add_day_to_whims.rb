class AddDayToWhims < ActiveRecord::Migration[5.2]
  def change
    add_column :whims, :day, :datetime
  end
end
