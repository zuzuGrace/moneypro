class AddDayToInvestments < ActiveRecord::Migration[5.2]
  def change
    add_column :investments, :day, :datetime
  end
end
