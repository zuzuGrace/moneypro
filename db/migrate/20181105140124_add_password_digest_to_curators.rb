class AddPasswordDigestToCurators < ActiveRecord::Migration[5.2]
  def change
    add_column :curators, :password_digest, :string
  end
end
