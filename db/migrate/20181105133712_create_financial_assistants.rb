class CreateFinancialAssistants < ActiveRecord::Migration[5.2]
  def change
    create_table :financial_assistants do |t|
      t.text :note
      t.string :numbered_value
      t.string :action

      t.timestamps
    end
  end
end
