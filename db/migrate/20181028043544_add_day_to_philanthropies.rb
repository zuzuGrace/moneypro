class AddDayToPhilanthropies < ActiveRecord::Migration[5.2]
  def change
    add_column :philanthropies, :day, :datetime
  end
end
