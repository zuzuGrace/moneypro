class AddDayToNecessities < ActiveRecord::Migration[5.2]
  def change
    add_column :necessities, :day, :datetime
  end
end
