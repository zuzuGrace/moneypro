class AddIndexToCuratorsUsername < ActiveRecord::Migration[5.2]
  def change
  	add_index :curators, :username, unique: true
  end
end
