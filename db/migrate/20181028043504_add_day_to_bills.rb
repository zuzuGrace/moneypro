class AddDayToBills < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :day, :datetime
  end
end
