class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :stripe_customer_id
      t.boolean :paid

      t.timestamps
    end
  end
end
