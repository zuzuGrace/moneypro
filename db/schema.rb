# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_08_014613) do

  create_table "balances", force: :cascade do |t|
    t.string "available_balance"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_balances_on_user_id"
  end

  create_table "bill_logs", force: :cascade do |t|
    t.integer "bill_id"
    t.string "amount_spent"
    t.string "type_of_spending"
    t.string "updated_balance"
    t.datetime "date_posted"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bill_id"], name: "index_bill_logs_on_bill_id"
  end

  create_table "bills", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "available_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "day"
    t.index ["user_id"], name: "index_bills_on_user_id"
  end

  create_table "curators", force: :cascade do |t|
    t.string "username"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["username"], name: "index_curators_on_username", unique: true
  end

  create_table "enrichment_logs", force: :cascade do |t|
    t.integer "enrichment_id"
    t.string "amount_spent"
    t.string "type_of_spending"
    t.string "updated_balance"
    t.datetime "date_posted"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["enrichment_id"], name: "index_enrichment_logs_on_enrichment_id"
  end

  create_table "enrichments", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "available_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "day"
    t.index ["user_id"], name: "index_enrichments_on_user_id"
  end

  create_table "financial_assistants", force: :cascade do |t|
    t.text "note"
    t.string "numbered_value"
    t.string "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "investment_logs", force: :cascade do |t|
    t.integer "investment_id"
    t.string "amount_spent"
    t.string "type_of_spending"
    t.string "updated_balance"
    t.datetime "date_posted"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["investment_id"], name: "index_investment_logs_on_investment_id"
  end

  create_table "investments", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "available_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "day"
    t.index ["user_id"], name: "index_investments_on_user_id"
  end

  create_table "necessities", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "available_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "day"
    t.index ["user_id"], name: "index_necessities_on_user_id"
  end

  create_table "necessity_logs", force: :cascade do |t|
    t.integer "necessity_id"
    t.string "amount_spent"
    t.string "type_of_spending"
    t.string "updated_balance"
    t.datetime "date_posted"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["necessity_id"], name: "index_necessity_logs_on_necessity_id"
  end

  create_table "philanthropies", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "available_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "day"
    t.index ["user_id"], name: "index_philanthropies_on_user_id"
  end

  create_table "philanthropy_logs", force: :cascade do |t|
    t.integer "philanthropy_id"
    t.string "amount_spent"
    t.string "type_of_spending"
    t.string "updated_balance"
    t.datetime "date_posted"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["philanthropy_id"], name: "index_philanthropy_logs_on_philanthropy_id"
  end

  create_table "securities", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "available_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "day"
    t.index ["user_id"], name: "index_securities_on_user_id"
  end

  create_table "security_logs", force: :cascade do |t|
    t.integer "security_id"
    t.string "amount_spent"
    t.string "type_of_spending"
    t.string "updated_balance"
    t.datetime "date_posted"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["security_id"], name: "index_security_logs_on_security_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "email"
    t.string "stripe_customer_id"
    t.boolean "paid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.string "balance"
    t.boolean "first_time"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "whim_logs", force: :cascade do |t|
    t.integer "whim_id"
    t.string "amount_spent"
    t.string "type_of_spending"
    t.string "updated_balance"
    t.datetime "date_posted"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["whim_id"], name: "index_whim_logs_on_whim_id"
  end

  create_table "whims", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "available_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "day"
    t.index ["user_id"], name: "index_whims_on_user_id"
  end

end
